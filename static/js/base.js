function htmlUser(firstName, lastBadgingTs, isNew) {
    if (lastBadgingTs) {
        tdClass = (isNew ? 'new' : 'present')
        html += '<td class=' + tdClass + '>' + firstName + '</td>'
        html += '<td class=' + tdClass + '>' + lastBadgingTs + '</td>'
        html += '<td class=' + tdClass + '>Present</td>'
    } else {
        tdClass = (isNew ? 'new' : 'absent')
        html += '<td class=' + tdClass + '>' + firstName + '</td>'
        html += '<td class=' + tdClass + '></td>'
        html += '<td class=' + tdClass + '>Absent</td>'
    }   
    return html
}

function renderUsers(data) {
    html = ''
    if (firstTime) {
        oldData = {}
        oldId = []
        for (user in data) {
            id = data[user]['id']
            oldId.push(id)
            oldData[id] = data[user]['last_badging_ts']
        }
        firstTime = false
    }
    for (user in data) {
        if (!(oldId.includes(data[user]['id']))) {
            isNew = true;
        } else {
            if (data[user]['last_badging_ts'] != oldData[data[user]['id']]) {
                isNew = true;
            } else {
                isNew = false
            }
        }
        html += '<tr>'
        htmlUser(data[user]['first_name'], data[user]['last_badging_ts'], isNew)
        html += '</tr>'
        isNew = false
    }
    $('#users').html(html)
}

$(document).ready(function () {
    token = $("[name='token']").val()
    firstTime = true
    setInterval(function () {
        $.ajax({
            method: "GET",
            url: "/api/user",
            headers: {'Authorization': 'token ' + token},
            success: renderUsers
        });
    }, 500);
});