import locale
from datetime import datetime

from flask import Flask, request
from flask import render_template
from flask_restful import Resource, Api
from flask_htpasswd import HtPasswdAuth
from flask_pymongo import PyMongo


app = Flask(__name__)

app.config["MONGO_URI"] = "mongodb://heroku_sj32k0vb:6qlvplt45held5i5ck4bsmhvii@ds147033.mlab.com:47033/heroku_sj32k0vb"
mongo = PyMongo(app)

app.config['FLASK_HTPASSWD_PATH'] = '.htpasswd'
app.config['FLASK_SECRET'] = 'test_secret'
htpasswd = HtPasswdAuth(app)

api = Api(app)

users = mongo.db['User']

locale.setlocale(locale.LC_ALL, "fr_FR")


class UserController(Resource):
    @htpasswd.required
    def get(self, user):
        users = [document for document in mongo.db.User.find().sort('id', -1)]
        for user in users:
            user.pop('_id')
        return users

    def post(self):
        user = request.get_json()
        try:
            assert 'first_name' in user
            assert 'last_name' in user
            latest_id = mongo.db.User.find().sort('id', -1)[0]['id']
        except AssertionError:
            return {"status": "ERROR", "message": "missing mandatory field", "error": "missing mandatory field"}, 400
        except IndexError:
            latest_id = -1
        user['id'] = latest_id + 1
        users.insert_one(user)
        return {"status": "SUCCESS", "message": None, "error": None, "id": user['id']}


class BadgingController(Resource):
    def post(self, user_id):
        user = users.find_one({"id": user_id})
        if not user:
            return {"status": "ERROR", "message": "incorrect id", "error": "incorrect id"}, 404
        post_data = request.get_json()
        if post_data and 'code' in post_data:
            user['last_code'] = post_data('code')
        users.update_one({"id": user_id}, {'$set': {'last_badging_ts': datetime.now().strftime("%A %d %B %H:%M")}})
        return {"status": "SUCCESS", "message": None, "error": None, "id": user_id}


api.add_resource(UserController, '/api/user')
api.add_resource(BadgingController, '/api/save_badging/<int:user_id>')


@app.route('/')
@htpasswd.required
def render_users(user):
    return render_template('index.html', token=htpasswd.generate_token(user))
